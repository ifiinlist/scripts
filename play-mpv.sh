#!/usr/bin/env sh

# This script plays any link with `mpv`.
# It uses `youtube-dl` or `yt-dlp` behind the scenes.
# Make sure to install these programs first.

# Send a notification.
# First check if the system has some kind of notification
# daemon installed. If succeeds then display a notification
# before playing the video.
if command -v notify-send > /dev/null; then
    notify-send " Play mpv" "Playing video ..."
fi

# Play the copied URL with mpv.
# I'm using two profiles here, ipc and yt.
# for more details on those profies check out my mpv
# config here:
# TODO: Add link to the mpv config here.
# If mpv fails to play the URL it will show a notification
# as a warning.
mpv --profile=ipc "$(wl-paste -n)" || notify-send " Play mpv" "Provided URL was not valid !!"

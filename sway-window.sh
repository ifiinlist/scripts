#!/usr/bin/env sh

# This script gets all open containers from sway tree.
# Then it formats the output and shows them as a list with fuzzel
# It lets the user choose a container. Then it switches focus to that
# chosen container.
# I'm using core utils on this one. But you could probably
# use something like `jq`. Because `swaymsg -t get_tree` returns
# output in json format. But I just like to use the hard method I guess.
windowId=$(swaymsg -t get_tree | grep -B8 -A26 '"border": "pixel",' | grep '"id"\|"name"' | cut -d ":" -f 2- | sed "s/^ //; s/,$//; s/^\"//; s/\"$//" | sed 'N;s/\n/\t/' | fuzzel --dmenu -p "Select window: " | awk '{print $1}')

# Switch to the selected window.
swaymsg [con_id="$windowId"] focus

#!/usr/bin/env bash

# This is a helper script to check if mpv is running with
# the ipc profile. For more info on the `ipc profile` look
# into my mpv config.

# Check if mpv is running with the ipc profile.
if [ $(pgrep -f "mpv --profile=ipc") ]; then
  # If it is running with ipc profile then start new mpv
  # without the profile and force it to spawn a window.
  # I'm forcing window to make sure the mpv doesn't just run
  # in the background with me having no direct access to it.
  # It's not a issue if the given media is a video, cause it'll
  # spawn a window anyway. But it's really useful for audio media.
  mpv --force-window "$@"
else
  # If mpv is not running with the ipc profile then, start
  # mpv with the ipc profile.
  mpv --profile=ipc "$@"
fi

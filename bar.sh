#!/usr/bin/env sh

# This is the script that generates my status bar.
# It uses functions, colors and icons 😉.
# This script is tailored to be used with `swaybar`.
# But really you can use it with whatever program
# that supports displaying text from a shell script.

# ⚠️ WARNING:
# This script generates bar with colors. The colors
# will only be visible if the the program being used
# as the bar supports pango markup. If used with `sway`
# then please enable pango markup for the bar.
# It also shows icons. So make sure you are using a
# font patched with Nerd Font.

# Colors needed for the bar.
# These colors are mostly from tokyonight theme.
black="#222436"
blue="#82aaff"
green="#c3e88d"
magenta="#c099ff"
red="#ff757f"
white="#c8d3f5"
yellow="#ffc777"

# Function to set the foreground color for an element.
# It takes the color value as an argument.
cf() {
  printf "<span foreground=\"%s\">" "$1"
}

# Function to set the background color for an element.
# It takes the color value as an argument.
cb() {
  printf "<span background=\"%s\">" "$1"
}

# Function for the closing tag of `cf` of `cb`.
cc() {
  printf "</span>"
}

# Function to show the current date and time on the bar.
# It will be displayed as "(DD Mon Day) HH:MM".
# So basically "(date month[in 3 letters] day of the week[3 letters]) current hour: curr minute".
# For more information on the format sequences (you know the %d, %b etc)
# check out the manpage for `date`.
date_and_time() {
  date '+(%d %b %a) %H:%M'
}

# Show remaining disk space of the root partition in MegaBytes.
# It uses `df` to get the disk info of the root partition.
# Then it uses `awk` to grab the 4th field. Which is the field
# with the remaining disk space info.
disk_free() {
  df -h / | awk 'END{print $4}'
}

# Show used ram space.
# It gets the output from `free -hL` which shows current ram/swap
# info in one line. The uses `awk` to get the 6th field, which
# displays the current used ram space info.
ram_free() {
  free -hL | awk '{print $6}'
}

# The next 3 functions are here to get info about currently playing
# media on mpv with the ipc server on /tmp/mpvsocket. My config
# automatically starts mpv with that ipc server. For more info on
# that check out my mpv config in my dotfiles repository. To get
# info from the ipc server we send a json command with `echo`
# and `socat`. ⚠️ NOTICE: Please make sure to install `socat`.
# It's not installed by default on most systems. After we send
# a json command to the server, it gives us a response. We take
# the response (which is in json format) and extract our intented
# data from the response with some GNU coreutils. This can also be
# done more easily and efficiently (I guess) with the `jq` program.
# But I like the hard way of using coreutils 😉.

# This function gets the currently playing media title from the
# ipc server.
mpv_file() {
  echo '{ "command": ["get_property", "path"] }' | socat - "/tmp/mpvsocket" | cut -d '"' -f4 | rev | cut -d "/" -f 1 | rev | cut -c -20
}

# This function gets the remaining time of the currently playing
# media from the ipc server.
mpv_time() {
  echo '{ "command": ["get_property", "time-remaining"] }' | socat - "/tmp/mpvsocket" | cut -d '"' -f 3 | sed 's/://g' | cut -d '.' -f 1 | awk '{printf "%d:%02d:%02d", $1/3600, $1/60, $1%60}'
}

# This function gets the volume from the ipc server.
mpv_volume() {
  echo '{ "command": ["get_property", "volume"] }' | socat - "/tmp/mpvsocket" | cut -d '"' -f 3 | sed 's/://g' | cut -d '.' -f 1
}

# This function checks if the system is connected to the internet
# with an ethernet cable. Depending on the result it displays
# different icons.
net_connected() {
  # Check the output of /sys/class/net/enp2s0/carrier
  # It either returns 1 if the system is connected to the internet
  # with a ethernet cable or returns 0 if the system is not connected.
  # Depending on your motherboard the file path may be different.
  # Check which interface exists on /sys/class/net/ . On my case
  # it was `enp2s0`. Check what your's is then modify the script.
  # ⚠️ NOTICE: This probably doesn't work with wireless connections.
  # I haven't tried with wireless connections yet.
  connection_status=$(cat /sys/class/net/enp2s0/carrier)
  if [ "$connection_status" -eq "1" ]; then
    echo "$(cf "$green")󰤨 $(cc)"
  else
    echo "$(cf "$red")󰤭 $(cc)"
  fi
}

# This function extracts the current speaker volume. It also checks
# if the speaker is muted. It uses `wireplumber` to get the output
# from. Also make sure to install `pipewire-pulse` for this to work.
speaker_volume() {
  # Check if the speaker is muted, if it is then it returns [muted].
  is_muted=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $3}')

  # Now check if is_muted is an empty string.
  if [ "$is_muted" = "" ]; then
    # If it is empty then display current speaker volume.
    echo "$(cf "$green")󰓃 $(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $2*100}')%$(cc)"
  else
    # If it is not empty then display muted icon.
    echo "$(cf "$red")󰓄$(cc) "
  fi
}

# This function extracts the current microphone volume. It also checks
# if the microphone is muted. It uses `wireplumber` to get the output
# from. Also make sure to install `pipewire-pulse` for this to work.
mic_volume() {
  # Check if the microphone is muted, if it is then it returns [muted].
  is_muted=$(wpctl get-volume @DEFAULT_AUDIO_SOURCE@ | awk '{print $3}')
  if [ "$is_muted" = "" ]; then
    # If it is empty then display current microphone volume.
    echo "$(cf "$green") $(wpctl get-volume @DEFAULT_AUDIO_SOURCE@ | awk '{print $2*100}')%$(cc)"
  else
    # If it is not empty then display muted icon.
    echo "$(cf "$red")$(cc) "
  fi
}

# The next couple of lines until the where start displying
# the actual bar, are used to display current bandwith speed.
# This is kind of a hard and hacky way of getting the
# current bandwith speed. This could have been done more
# easily with other tools. But as I have stated before
# I like complex and hard ways 😉. This script displays
# bandwith speed in KilloBytes (to the power of 1024) or
# MegaBytes. On GNU/Linux filesystems /proc/net/dev holds
# current computing session's bandwith usage info.
# The file has this info in bytes. And it only includes
# Total bytes of data received or sent from the device.
# This script takes that info and basically subtracts it
# from the original value each second to get the bandwith
# usage. Then it converts the Bytes to KB or MB.

# Get initial value.
initial_receive=$(awk 'END{print $2}' < /proc/net/dev)
initial_transmit=$(awk 'END{print $10}' < /proc/net/dev)

# Start the 1 second loop. This loop also holds the actual bar.
while true; do
  # Get new value each second.
  later_receive=$(awk 'END{print $2}' < /proc/net/dev)
  later_transmit=$(awk 'END{print $10}' < /proc/net/dev)

  # Calculate bandwith usage in Bytes each second.
  receive="$(((later_receive - initial_receive) / 1024))"
  transmit="$(((later_transmit - initial_transmit) / 1024))"

  # Reassign the later values to the inital value.
  initial_receive="$later_receive"
  initial_transmit="$later_transmit"

  # Convert recieved bytes to KiloBytes or MegaBytes.
  if [ "$receive" -gt "1024" ]; then
    receive="$(echo $(((receive * 10) / 1024)) | sed -e 's/.$/.&/')M"
  else
    receive="$((receive))K"
  fi

  # Convert transmitted bytes to KilloBytes or MegaBytes.
  if [ "$transmit" -gt "1024" ]; then
    transmit="$(echo $(((transmit * 10) / 1024)) | sed -e 's/.$/.&/')M"
  else
    transmit="$((transmit))K"
  fi

  # Start the actual bar
  # Check if mpv is running with the ipc profile.
  # Depending on the result the script displays the bar with
  # or without mpv info.
  if [ $(pgrep -f "mpv --profile=ipc") ]; then
    # The info is displayed in this order:
    # <bandwith usage> <network connction> <mpv info> <disk usage> <ram usage> <speaker> <microphone> <time and date>
    # It has some colors and icons.
    echo "\
$(cf "$yellow") $receive  $transmit$(cc) \
$(net_connected) \
 $(cb "$black")$(cf "$white") 󰎆  $(mpv_file).. 󱎫  $(mpv_time)   $(mpv_volume)% $(cc)$(cc)  \
$(cf "$magenta")  $(disk_free)$(cc) \
$(cf "$magenta")󰍛 $(ram_free)$(cc)  \
$(speaker_volume) \
$(mic_volume)  \
$(cb "$blue")$(cf "$black") 󰃭 $(date_and_time) $(cc)$(cc)\
"
  else
    # The info is displayed in this order:
    # <bandwith usage> <network connction> <disk usage> <ram usage> <speaker> <microphone> <time and date>
    # It has some colors and icons.
    echo "\
$(cf "$yellow") $receive  $transmit$(cc) \
$(net_connected)  \
$(cf "$magenta")  $(disk_free)$(cc) \
$(cf "$magenta")󰍛 $(ram_free)$(cc)  \
$(speaker_volume) \
$(mic_volume)  \
$(cb "$blue")$(cf "$black") 󰃭 $(date_and_time) $(cc)$(cc)\
"
  fi
  # Run the loop each second.
  sleep 1s
done

#!/usr/bin/env sh

# This script adds copied URL to the bookmark file.
# It checks if the book mark already exist. It also
# sorts and removes duplicates from the bookmark file.

# Save the copied URL in a variable. The `-n` flag
# with `wl-paste` trims trailing new lines from the URL.
link=$(wl-paste -n)

# Save the bookmark file path in a variable
file="$HOME/null-void/private/bookmarks.txt"

# This file here is to copy the main bookmark file
# then sort and remove duplicate + empty lines from it.
# `.XXXX` after altbookmark means any random 4 characters.
filealt="$(mktemp /tmp/altbookmark.XXXX)"

# This function sorts the bookmark file, removes duplicates
# and removes empty lines from the file.
sort_remove_dup_and_empty_line() {
  # Copy the original bookmark file to the temporary one.
  cp "$file" "$filealt"

  # Sort the temporary file, make it unique, remove blank lines
  # Then write it to the original bookmark file.
  sort "$filealt" | uniq | sed '/^ *$/d' > "$file"

  # Remove the temporary file.
  rm "$filealt"
}

# This function just adds the copied URL at the end of the bookmark file.
add_bookmark() {
  # Add the bookmark to the specified file.
  echo "$link" >> "$file"

  # Notify the user that the bookmark has been added.
  notify-send "󰂺 Bookmark" "Bookmark added !!"
}

# Cleanify the bookmark file before doing anything.
sort_remove_dup_and_empty_line

# Check if the copied URL is empty or not.
if [ "$link" = "" ]; then
  # If the URL is empty, notify the user.
  notify-send " Bookmark" "Cannot add empty bookmark !!"
else
  # If the URL is not empty then check if the URL already exists
  # in the bookmark file.
  # `grep -x` check for exact line match.
  if grep -x "$link" "$file"; then
    # If the URL already exists as a bookmark then notify the
    # user and abort.
    notify-send " Bookmark" "Bookmark already exist !!"
    notify-send " Bookmark" "Bookmark was not added !!"
    exit 1
  else
    # If the URL doesn't exist as a bookmark then add it as a
    # new bookmark and cleanup the bookmark file again.
    add_bookmark
    sort_remove_dup_and_empty_line
  fi
fi

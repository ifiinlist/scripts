#!/usr/bin/env bash

# This script fixes a very annoying problem I have with
# `swayimg` v2.0
# `swayimg` has been my image viewer for a long time. It has
# a feature to integrate itself with swaywm. But I don't like it.
# It floats the swayimg window on top of every thing else.
# To disable this "feature" there was a config option `sway = no`.
# But that option was removed in v2.0 . Now this is the workaround.

# Unsetting SWAYSOCK to fool swayimg to thinking it's not in swaywm.
unset SWAYSOCK

# Starting `swayimg` with given args from command.
swayimg "$@"
